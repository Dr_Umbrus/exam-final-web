//Array des objets dans la liste de courses
courses = [];

//Array des prix
prix = [];

//Array de quantités
quantite = [];




function AddItem()
{

	name = document.getElementById("name").value;
	price = document.getElementById("price").value;
	quantity=document.getElementById("quantity").value


	//On vérifie que la quantité et le prix sont bien des nombres.
	if(isNaN(price)===false && isNaN(quantity)===false)
	{
		verif = true;

		for(var i=0; i<courses.length; i++)
		{
			if(courses[i]===name)
			{
				prix[i]=price;
				quantite[i]=Number(quantite[i])+Number(quantity);
				verif=false;
			}
		}
		
		if(verif===true)
		{
			console.log("meh");
			courses.push(name);
		    prix.push(price);
		    quantite.push(quantity);
		}
		

		console.log(courses);
		console.log(quantite);
		console.log(prix)
		UpdateList();
	}

	return false

}

/*
//Fonction pour modifier les valeurs directement depuis la liste. Actuellement non fonctionnel.
function ModifValue(){

	//On prend chaque id, et on met à jour la quantité et le prix, si on a rentré des valeurs en chiffres.
	for(var i=0; i<courses.length; i++)
	{
		if(isNaN(price)===false && isNaN(quantity)===false)
		{
    		price = document.getElementById("price"+i).value;
			quantity=document.getElementById("quantity"+i).value
			quantite[i]=quantity;
 			prix[i]=Number(price)*Number(quantity);
		}
	}
}
*/

//fonction pour retirer une ligne
function DeleteLine(del_id)
{
	//On sélectionne l'id de la ligne à retirer
	var deleted=Number(del_id);

	//Puis on enlève cet id de tous les tableaux
	courses.splice(deleted,1);
	prix.splice(deleted,1);
	quantite.splice(deleted,1);

	//Et on met à jour la liste
	UpdateList();
}

//Met à jour la liste avec les informations souhaitées
function UpdateList()
{

	//Récupérer tout le contenu des nodes
 	var list = document.querySelector("#mainList");

	//Puis les détruire en partant de la fin
 	var child = list.lastElementChild;
 	while(child)
 	{
 		list.removeChild(child);
 		child=list.lastElementChild;
 	}

	//On recréé ensuite les survivants
	for(var i=0; i<courses.length; i++)
	{
		//On créé une nouvelle row
		var addRow = document.createElement("div");
		addRow.classList.add("row");

		//La colonne avec les indexs
    	var addDivIndex = document.createElement("div");
    	addDivIndex.classList.add("col-1");
    	var addPIndex = document.createElement("p");
    	addPIndex.appendChild(document.createTextNode(i));
    	addDivIndex.appendChild(addPIndex);

    	//La colonne avec les noms
		var addDivName = document.createElement("div");
		addDivName.classList.add("col-4");
		var addPName = document.createElement("p");
		addPName.appendChild(document.createTextNode(courses[i]));
		addDivName.appendChild(addPName);

		//La colonne des quantités
		var addDivQt = document.createElement("div");
		addDivQt.classList.add("col-1");
		var addPQt = document.createElement("p");
		addPQt.appendChild(document.createTextNode(quantite[i]));
		addDivQt.appendChild(addPQt);

		//La colonne des prix
		var addDivPrice = document.createElement("div");
		addDivPrice.classList.add("col-2");
		var addPPrice = document.createElement("p");
		addPPrice.appendChild(document.createTextNode(Number(prix[i])*Number(quantite[i])));
		addDivPrice.appendChild(addPPrice);

		//La colonne check
		var addDivCheck = document.createElement("div");
		addDivCheck.classList.add("col-1");
		var addCheckBox = document.createElement("input");
		addCheckBox.type="checkbox";
		addCheckBox.id="check"+i;
		addDivCheck.appendChild(addCheckBox);

		//La colonne supprimer
		var addDivButt = document.createElement("div");
		addDivButt.classList.add("col-3");
		var addButton = document.createElement("button");
		addButton.id=i;
		addButton.classList.add("btn-danger");
		addButton.appendChild(document.createTextNode("Supprimer"));
		addButton.onclick = function(){DeleteLine(this.id)}
		addDivButt.appendChild(addButton);

		//Ajouter tout ça au html
		var addList = document.getElementById("mainList");
		addList.appendChild(addDivIndex);
		addList.appendChild(addDivName);
		addList.appendChild(addDivQt);
		addList.appendChild(addDivPrice);
		addList.appendChild(addDivCheck);
		addList.appendChild(addDivButt);
	}

	//On calcule ensuite le total
	SetTotal();
}


//Fonction pour calculer le prix total
function SetTotal()
{
	//Récupérer tout le contenu des nodes
 	var list = document.querySelector("#total");

	//Puis les détruire en partant de la fin
 	var child = list.lastElementChild;
 	while(child)
 	{
 	list.removeChild(child);
 	child=list.lastElementChild;
	}

	//Une variable pour stocker l'information
	var total=0;

	//On effectue une somme de tous les prix totaux
	for (var i = 0; i <courses.length; i++) 
	{
	total=Number(total)+(Number(prix[i])*Number(quantite[i]));	
	}

	//on récupère la dic où est noté le total
	var addTot = document.getElementById("total");
	//Puis on créé  le paragraphe
	var addPTot = document.createElement("p");
	//La valeur est ensuite ajouée au paragraphe, dans la div appropriée.
	addPTot.appendChild(document.createTextNode(total));
	addTot.appendChild(addPTot);
}